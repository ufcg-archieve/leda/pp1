package main.java.problems;

public class CountGreaterThanImpl implements CountGreaterThan {
    
    public int countGreater(Integer[] array, Integer x) {
        int maiores = 0;

        if (array != null && array.length > 0 && x != null) {
            int primeiroMaior = buscaPrimeiroMaior(array, x, 0, array.length - 1);
            if (primeiroMaior != -1) {
                maiores = array.length - primeiroMaior;
            }
        }
           
        return maiores;
    }

    private int buscaPrimeiroMaior(Integer[] array, Integer x, int left, int right) {
        int index = -1;
        if (left <= right) {
            int middle = (left + right) / 2;
            if (array[middle].compareTo(x) > 0) {
                if (middle > 0 && array[middle - 1].compareTo(x) > 0) {
                    index = buscaPrimeiroMaior(array, x, left, middle - 1);
                } else {
                    index = middle;
                }
            } else {
                index = buscaPrimeiroMaior(array, x, middle + 1, right);
            }
        }
    
        return index;
    }
}
